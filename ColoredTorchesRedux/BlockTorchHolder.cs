using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Util;

namespace ColoredTorchesRedux
{
	public class BlockTorchHolder : Block
	{
		public bool Empty
		{
			get
			{
				return this.Variant["state"] == "empty";
			}
		}

		public override bool OnBlockInteractStart(IWorldAccessor world, IPlayer byPlayer, BlockSelection blockSel)
		{
			if (this.Empty)
			{
				ItemStack heldStack = byPlayer.InventoryManager.ActiveHotbarSlot.Itemstack;
				if (heldStack != null) {
          StringBuilder stringBuilder = new StringBuilder();
          string torchHolderVariantName = "filled";

          if(!heldStack.Collectible.Code.Path.Equals("torch-basic-lit-up")) {
            string regex = @"coloredtorch-basic-(\w+)-lit-up";
            Regex r = new Regex(regex, RegexOptions.IgnoreCase);
            Match match = r.Match(heldStack.Collectible.Code.Path);
            torchHolderVariantName = $"filled{match.Groups[1].Value}";
          }

					byPlayer.InventoryManager.ActiveHotbarSlot.TakeOut(1);
					byPlayer.InventoryManager.ActiveHotbarSlot.MarkDirty();
					Block filledBlock = world.GetBlock(base.CodeWithVariant("state", torchHolderVariantName));
					world.BlockAccessor.ExchangeBlock(filledBlock.BlockId, blockSel.Position);
					BlockSounds sounds = this.Sounds;
					if (((sounds != null) ? sounds.Place : null) != null)
					{
						world.PlaySoundAt(this.Sounds.Place, (double)blockSel.Position.X, (double)blockSel.Position.Y, (double)blockSel.Position.Z, byPlayer, true, 32f, 1f);
					}
					return true;
        }
			}
			else
			{
        ItemStack stack;
        if (this.Variant["state"].Equals("filled")) {
				  stack = new ItemStack(world.GetBlock(new AssetLocation("torch-basic-lit-up")), 1);
        } else {
          string regex = @"filled(\w+)";
          Regex r = new Regex(regex, RegexOptions.IgnoreCase);
          Match match = r.Match(this.Variant["state"]);
				  stack = new ItemStack(world.GetBlock(new AssetLocation($"coloredtorches:coloredtorch-basic-{match.Groups[1].Value}-lit-up")), 1);
        }

				if (byPlayer.InventoryManager.TryGiveItemstack(stack, true))
				{
					Block filledBlock2 = world.GetBlock(base.CodeWithVariant("state", "empty"));
					world.BlockAccessor.ExchangeBlock(filledBlock2.BlockId, blockSel.Position);
					BlockSounds sounds2 = this.Sounds;
					if (((sounds2 != null) ? sounds2.Place : null) != null)
					{
						world.PlaySoundAt(this.Sounds.Place, (double)blockSel.Position.X, (double)blockSel.Position.Y, (double)blockSel.Position.Z, byPlayer, true, 32f, 1f);
					}
					return true;
				}
			}
			return base.OnBlockInteractStart(world, byPlayer, blockSel);
		}

		public override WorldInteraction[] GetPlacedBlockInteractionHelp(IWorldAccessor world, BlockSelection selection, IPlayer forPlayer)
		{
			if (this.Empty)
			{
				return new WorldInteraction[]
				{
					new WorldInteraction
					{
						ActionLangCode = "blockhelp-torchholder-addtorch",
						MouseButton = EnumMouseButton.Right,
						Itemstacks = new ItemStack[]
						{
							new ItemStack(world.GetBlock(new AssetLocation("torch-basic-lit-up")), 1),
							new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-basic-blue-lit-up")), 1),
							new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-basic-cyan-lit-up")), 1),
							new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-basic-green-lit-up")), 1),
							new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-basic-orange-lit-up")), 1),
							new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-basic-pink-lit-up")), 1),
							new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-basic-purple-lit-up")), 1),
							new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-basic-red-lit-up")), 1),
							new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-basic-yellow-lit-up")), 1)
						}
					}
				}.Append(base.GetPlacedBlockInteractionHelp(world, selection, forPlayer));
			}
			return new WorldInteraction[]
			{
				new WorldInteraction
				{
					ActionLangCode = "blockhelp-torchholder-removetorch",
					MouseButton = EnumMouseButton.Right,
					Itemstacks = null
				}
			}.Append(base.GetPlacedBlockInteractionHelp(world, selection, forPlayer));
		}
	}
}
