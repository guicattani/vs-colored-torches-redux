﻿using Vintagestory.API.Common;

namespace ColoredTorchesRedux
{
    public class ColoredTorchesReduxModSystem : ModSystem
    {
        public override void Start(ICoreAPI api)
        {
            base.Start(api);
            api.RegisterBlockClass("BlockTorchHolder", typeof(BlockTorchHolder));
        }
    }
}
